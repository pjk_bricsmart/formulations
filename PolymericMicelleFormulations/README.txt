This workflow was developed on Ubuntu 16.04.6 LTS using the following software and package versions:

Software:
	KNIME v. 3.4.2
	R v. 3.4.4

R packages:
	qsarR v. 1.6.4
	qsarm v. 1.5
	caret v. 6.0-80
	lattice v. 0.20-35
	ggplot2 v. 3.0.0
	car v. 3.0-2
	plyr v. 1.8.4